# README #

Experiment to generate llvm IR, convert that into assembly code, then compile and link the assembly code.

The interesting stuff is in build.sh. Later we can figure out how to convert build.sh to a makefile.

build.sh will produce a.out which is an executable that will run on the msp430.

This project assumes that the msp430-gcc toolchain is installed at $HOME/ti/msp430_gcc.

I mostly followed this article for this experiment:
http://blitzfunk.com/2014/03/28/facepalm-into-clang-llvm-and-llvm/