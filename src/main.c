#include <msp430.h>

int main()
{
    /* Hold the watchdog */
    WDTCTL = WDTPW + WDTHOLD;

    /* Set P1.0 direction to output */
    P1DIR |= 0x01;

    /* Set P1.0 output high */
    P1OUT |= 0x01;

    unsigned int i = 0;

    while (1) {
        /* brute force delay */
        i = 40000;
        while (i-- > 0){}
        
        /* Toggle P1.0 output */
        P1OUT ^= 0x01;
    }
}