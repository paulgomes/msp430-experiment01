#compile main.c to llvm IR
/usr/bin/clang-3.5 -I$HOME/ti/msp430_gcc/include/ -D__MSP430G2553__ -g -S -emit-llvm -Wall -c src/main.c -o src/main.ll

# generate msp430 assembly code from main.ll
/usr/bin/llc-3.5 -march=msp430 -asm-verbose src/main.ll -o src/main.s

# compile main.s assembly code
$HOME/ti/msp430_gcc/bin/msp430-elf-gcc -Wall -D_GNU_ASSEMBLER_ -I$HOME/ti/msp430_gcc/include/ -mmcu=msp430g2553 -mcpu=430 -x assembler -Wa,-gstabs -c src/main.s -o src/main.o

# link main.o to create a.out. Also generate a map to see how everything is linked
$HOME/ti/msp430_gcc/bin/msp430-elf-gcc -L$HOME/ti/msp430_gcc/include -mmcu=msp430g2553 -mcpu=430 -Wl,-Map=a.out.map src/main.o -o a.out
